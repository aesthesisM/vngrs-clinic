package com.maygul.clinic.mapper;

import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.service.dto.DoctorDto;
import com.maygul.clinic.service.dto.SearchDoctorDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoctorMapper {

    DoctorDto entityToDto(Doctor entity);

    SearchDoctorDto entityToSearchDto(Doctor entity);

    List<DoctorDto> entityListToDtoList(List<Doctor> entityList);

    List<SearchDoctorDto> entityListToSearchDtoList(List<Doctor> entityList);
}
