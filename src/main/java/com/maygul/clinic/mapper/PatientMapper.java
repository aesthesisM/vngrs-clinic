package com.maygul.clinic.mapper;

import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.service.dto.PatientDto;
import com.maygul.clinic.service.dto.SearchPatientDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {

    PatientDto entityToDto(Patient entity);

    SearchPatientDto entityToSearchDto(Patient entity);

    List<PatientDto> entityListToDtoList(List<Patient> entityList);

    List<SearchPatientDto> entityListToSearchDtoList(List<Patient> entityList);

}
