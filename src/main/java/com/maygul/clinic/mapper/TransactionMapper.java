package com.maygul.clinic.mapper;

import com.maygul.clinic.persistence.entity.Transaction;
import com.maygul.clinic.service.dto.SearchTransactionDto;
import com.maygul.clinic.service.dto.TransactionDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    TransactionDto entityToDto(Transaction entity);

    SearchTransactionDto entityToSearchDto(Transaction entity);

    List<TransactionDto> entityListToDtoList(List<Transaction> entityList);

    List<SearchTransactionDto> entityListToSearchDtoList(List<Transaction> entityList);
}
