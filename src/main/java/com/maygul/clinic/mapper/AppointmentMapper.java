package com.maygul.clinic.mapper;

import com.maygul.clinic.persistence.entity.Appointment;
import com.maygul.clinic.service.dto.AppointmentDto;
import com.maygul.clinic.service.dto.SearchAppointmentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AppointmentMapper {

    @Mappings(value = {
            @Mapping(target = "doctorId", source = "doctor.id"),
            @Mapping(target = "patientId", source = "patient.id"),
            @Mapping(target = "status", source = "status")
    })
    AppointmentDto entityToDto(Appointment entity);

    @Mappings(value = {
            @Mapping(target = "doctorId", source = "doctor.id"),
            @Mapping(target = "patientId", source = "patient.id"),
            @Mapping(target = "status", source = "status")
    })
    SearchAppointmentDto entityToSearchDto(Appointment entity);

    List<SearchAppointmentDto> entityListToSearchDtoList(List<Appointment> entityList);
}
