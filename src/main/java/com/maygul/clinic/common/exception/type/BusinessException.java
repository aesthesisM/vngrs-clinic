package com.maygul.clinic.common.exception.type;


import com.maygul.clinic.common.exception.data.ExceptionData;

/**
 * Http Status 500, Internal Server Error
 */
public class BusinessException extends MicroException {
  
  public BusinessException(ExceptionData data) {
    super(data);
  }
  
  public BusinessException(Long errorCode) {
    super(errorCode);
  }
  
  public BusinessException(Long errorCode, String message) {
    super(errorCode, message);
  }
}
