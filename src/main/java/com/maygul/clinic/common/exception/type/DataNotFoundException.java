package com.maygul.clinic.common.exception.type;

import com.maygul.clinic.common.exception.data.ExceptionData;

/**
 * Http Status 404, Not Found
 */
public class DataNotFoundException extends MicroException {

    public DataNotFoundException(ExceptionData data) {
        super(data);
    }
    
    public DataNotFoundException(Long errorCode) {
        super(errorCode);
    }

    public DataNotFoundException(Long errorCode, String message) {
        super(errorCode, message);
    }
}
