package com.maygul.clinic.common.exception.handler;


import com.maygul.clinic.common.exception.constant.MicroErrorType;
import com.maygul.clinic.common.exception.data.ExceptionData;
import com.maygul.clinic.common.exception.type.DataNotFoundException;
import com.maygul.clinic.common.exception.type.MicroException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Locale;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class MicroExceptionHandler extends ResponseEntityExceptionHandler {
  
  private final Logger log = LoggerFactory.getLogger(MicroExceptionHandler.class);
  
  @Value("${spring.application.name}")
  public String applicationName;
  
  @Autowired
  private MessageSource messageSource;
  
  private static final String GENERIC_ERROR_MESSAGE_KEY =
      "com.maygul.clinic.generic.exception.message";
  
  public MicroExceptionHandler() {
  }
  
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler(value = DataNotFoundException.class)
  public ExceptionData handleDataNotFoundException(DataNotFoundException ex, Locale locale) {
    return errorResponse(ex, ex.getExceptionData(), locale);
  }
  
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(value = MicroException.class)
  public ExceptionData handleMicroException(MicroException ex, Locale locale) {
    return errorResponse(ex, ex.getExceptionData(), locale);
  }
  
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(value = UndeclaredThrowableException.class)
  public ExceptionData handleUndeclaredThrowableException(UndeclaredThrowableException ex,
                                                          Locale locale) {
    if (ex.getCause() instanceof MicroException) {
      MicroException exception = (MicroException) ex.getCause();
      return errorResponse(ex, exception.getExceptionData(), locale);
    }
    ExceptionData exceptionData =
        new ExceptionData(MicroErrorType.INTERNAL_ERROR.getCode(), ex.getMessage());
    return errorResponse(ex, exceptionData, locale);
  }
  
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(value = Exception.class)
  public ExceptionData handleException(Exception ex, Locale locale) {
    ExceptionData exceptionData =
        new ExceptionData(MicroErrorType.INTERNAL_ERROR.getCode(), ex.getMessage());
    return errorResponse(ex, exceptionData, locale);
  }
  
  private ExceptionData errorResponse(Exception ex, ExceptionData exceptionData, Locale locale) {
    log.error("Exception handler caught an error:" + ExceptionUtils.getStackTrace(ex));
    populateExceptionDataWithServiceInformation(exceptionData, locale, ex);
    return exceptionData;
  }
  
  private void populateExceptionDataWithServiceInformation(ExceptionData exceptionData,
                                                           Locale locale, Exception ex) {
    if (exceptionData != null) {
      if (StringUtils.isEmpty(exceptionData.getApplicationName())) {
        exceptionData.setApplicationName(applicationName);
      }
      if (!StringUtils.isEmpty(exceptionData.getErrorMessage())) {
        String formattedMessage = null;
        try {
          formattedMessage =
              messageSource.getMessage(exceptionData.getErrorMessage(), null, locale);
        } catch (NoSuchMessageException ex2) {
        }
        if (formattedMessage == null) {
          formattedMessage = messageSource.getMessage(GENERIC_ERROR_MESSAGE_KEY, null, locale);
        }
        exceptionData.setErrorMessage(
            formattedMessage != null ? formattedMessage : exceptionData.getErrorMessage());
      }
    }
  }
}

