package com.maygul.clinic.common.exception.type;

import com.maygul.clinic.common.exception.data.ExceptionData;

public class MicroException extends Exception {
    
    private ExceptionData data;
    
    public MicroException(ExceptionData data) {
        super(data.getErrorMessage());
        this.data = data;
    }
    
    public MicroException(Long errorCode, String message) {
        this(new ExceptionData(errorCode, message));
    }
    
    public MicroException(Long errorCode){
        this(new ExceptionData(errorCode));
    }
    
    public ExceptionData getExceptionData() {
        return data;
    }
}
