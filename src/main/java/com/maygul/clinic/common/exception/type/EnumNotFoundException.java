package com.maygul.clinic.common.exception.type;

public class EnumNotFoundException extends DataNotFoundException {

    public EnumNotFoundException(String message) {
        super(1L, message);
    }
}
