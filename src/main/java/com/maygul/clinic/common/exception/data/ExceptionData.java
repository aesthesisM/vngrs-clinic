package com.maygul.clinic.common.exception.data;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ExceptionData implements Serializable {

    private String applicationName;
    private Long errorCode;
    private String errorMessage;

    public ExceptionData() {
    }

    public ExceptionData(Long errorCode) {
        this.errorCode = errorCode;
    }

    public ExceptionData(Long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ExceptionData(String applicationName, Long errorCode, String errorMessage) {
        this.applicationName = applicationName;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[").
                append("applicationName:").
                append(applicationName).
                append(",").
                append("errorCode:").
                append(errorCode).
                append(",").
                append("errorMessage:").
                append(errorMessage).
                append("]");
        return builder.toString();
    }
}
