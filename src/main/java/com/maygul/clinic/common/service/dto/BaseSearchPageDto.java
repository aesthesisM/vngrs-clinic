package com.maygul.clinic.common.service.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BaseSearchPageDto<T> {
    private List<T> pageResponse;
    private Long totalCount;
}