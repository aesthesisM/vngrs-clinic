package com.maygul.clinic.service;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.exception.PatientNotFoundException;
import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.specification.criteria.PatientCriteria;
import com.maygul.clinic.service.dto.PatientDto;
import com.maygul.clinic.service.dto.SearchPagePatientDto;

public interface PatientService {

    SearchPagePatientDto search(Integer pageNumber, Integer pageSize, PatientCriteria criteria);

    PatientDto getById(Long id) throws PatientNotFoundException;

    PatientDto save(String name, boolean isPatientDoctor, Long doctorId) throws DoctorNotFoundException;

    PatientDto update(Long id, String name) throws PatientNotFoundException;

    void delete(Long id) throws PatientNotFoundException;

    Patient getEntityById(Long id) throws PatientNotFoundException;
}
