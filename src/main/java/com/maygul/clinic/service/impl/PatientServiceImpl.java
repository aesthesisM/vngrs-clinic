package com.maygul.clinic.service.impl;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.exception.PatientNotFoundException;
import com.maygul.clinic.mapper.PatientMapper;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.entity.PatientType;
import com.maygul.clinic.persistence.repository.PatientRepository;
import com.maygul.clinic.persistence.specification.PatientSpecification;
import com.maygul.clinic.persistence.specification.criteria.PatientCriteria;
import com.maygul.clinic.service.DoctorService;
import com.maygul.clinic.service.PatientService;
import com.maygul.clinic.service.dto.PatientDto;
import com.maygul.clinic.service.dto.SearchPagePatientDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PatientServiceImpl implements PatientService {

    private PatientRepository patientRepository;
    private PatientMapper patientMapper;
    private DoctorService doctorService;

    public PatientServiceImpl(PatientRepository patientRepository,
                              PatientMapper patientMapper,
                              DoctorService doctorService) {
        this.patientRepository = patientRepository;
        this.patientMapper = patientMapper;
        this.doctorService = doctorService;
    }

    @Override
    public SearchPagePatientDto search(Integer pageNumber, Integer pageSize, PatientCriteria criteria) {
        Specification<Patient> specification = PatientSpecification.findByCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page page = patientRepository.findAll(specification, pageable);

        List<Patient> content = page.getContent();
        long totalCount = page.getTotalElements();

        SearchPagePatientDto dto = new SearchPagePatientDto(patientMapper.entityListToSearchDtoList(content), totalCount);
        return dto;
    }

    @Override
    public PatientDto getById(Long id) throws PatientNotFoundException {
        Patient patient = getEntityById(id);
        PatientDto dto = patientMapper.entityToDto(patient);
        return dto;
    }

    @Override
    public PatientDto save(String name, boolean isPatientDoctor, Long doctorId) throws DoctorNotFoundException {
        Patient patient = new Patient();
        patient.setName(name);

        if (isPatientDoctor) {
            Doctor doctor = doctorService.getEntityById(doctorId);
            patient.setDoctor(doctor);
            patient.setPatientType(PatientType.DOCTOR);
        } else {
            patient.setPatientType(PatientType.PATIENT);
        }

        patientRepository.save(patient);
        PatientDto dto = patientMapper.entityToDto(patient);
        return dto;
    }

    @Override
    @Transactional
    public PatientDto update(Long id, String name) throws PatientNotFoundException {
        Patient patient = getEntityById(id);
        patient.setName(name);
        patientRepository.save(patient);

        PatientDto dto = patientMapper.entityToDto(patient);
        return dto;
    }

    @Override
    @Transactional
    public void delete(Long id) throws PatientNotFoundException {
        Patient patient = getEntityById(id);
        patientRepository.delete(patient);
    }

    public Patient getEntityById(Long id) throws PatientNotFoundException {
        Patient patient = patientRepository.findById(id).orElseThrow(PatientNotFoundException::new);
        return patient;
    }
}
