package com.maygul.clinic.service.impl;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.mapper.DoctorMapper;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.repository.DoctorRepository;
import com.maygul.clinic.persistence.specification.DoctorSpecification;
import com.maygul.clinic.persistence.specification.criteria.DoctorCriteria;
import com.maygul.clinic.service.DoctorService;
import com.maygul.clinic.service.dto.DoctorDto;
import com.maygul.clinic.service.dto.SearchPageDoctorDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    private DoctorRepository doctorRepository;
    private DoctorMapper doctorMapper;

    public DoctorServiceImpl(DoctorRepository doctorRepository,
                             DoctorMapper doctorMapper) {
        this.doctorRepository = doctorRepository;
        this.doctorMapper = doctorMapper;
    }

    @Override
    public SearchPageDoctorDto search(Integer pageNumber, Integer pageSize, DoctorCriteria criteria) {
        Specification<Doctor> specification = DoctorSpecification.findByCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page page = doctorRepository.findAll(specification, pageable);

        List<Doctor> content = page.getContent();
        long totalCount = page.getTotalElements();

        SearchPageDoctorDto dto = new SearchPageDoctorDto(doctorMapper.entityListToSearchDtoList(content), totalCount);
        return dto;
    }

    @Override
    public DoctorDto getById(Long id) throws DoctorNotFoundException {
        Doctor doctor = getEntityById(id);
        DoctorDto dto = doctorMapper.entityToDto(doctor);
        return dto;
    }

    @Override
    public DoctorDto save(String name, String speciality, Double hourlyRate) {
        Doctor doctor = new Doctor();
        doctor.setName(name);
        doctor.setSpeciality(speciality);
        doctor.setHourlyRate(hourlyRate);

        doctorRepository.save(doctor);
        DoctorDto dto = doctorMapper.entityToDto(doctor);
        return dto;
    }

    @Override
    public DoctorDto update(Long id, String name, String speciality, Double hourlyRate) throws DoctorNotFoundException {
        Doctor doctor = getEntityById(id);
        if (name != null)
            doctor.setName(name);
        if (speciality != null)
            doctor.setSpeciality(speciality);
        if (hourlyRate != null)
            doctor.setHourlyRate(hourlyRate);

        doctorRepository.save(doctor);

        DoctorDto dto = doctorMapper.entityToDto(doctor);
        return dto;
    }

    @Override
    public void delete(Long id) throws DoctorNotFoundException {
        Doctor doctor = getEntityById(id);
        doctorRepository.delete(doctor);
    }

    @Override
    public Doctor getEntityById(Long id) throws DoctorNotFoundException {
        Doctor doctor = doctorRepository.findById(id).orElseThrow(DoctorNotFoundException::new);
        return doctor;
    }
}
