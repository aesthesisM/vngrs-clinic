package com.maygul.clinic.service.impl;

import com.maygul.clinic.exception.*;
import com.maygul.clinic.mapper.AppointmentMapper;
import com.maygul.clinic.persistence.entity.*;
import com.maygul.clinic.persistence.repository.AppointmentRepository;
import com.maygul.clinic.persistence.repository.TransactionRepository;
import com.maygul.clinic.persistence.specification.AppointmentSpecification;
import com.maygul.clinic.persistence.specification.criteria.AppointmentCriteria;
import com.maygul.clinic.service.AppointmentService;
import com.maygul.clinic.service.DoctorService;
import com.maygul.clinic.service.PatientService;
import com.maygul.clinic.service.dto.AppointmentDto;
import com.maygul.clinic.service.dto.SearchPageAppointmentDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentRepository appointmentRepository;
    private AppointmentMapper appointmentMapper;
    private DoctorService doctorService;
    private PatientService patientService;
    private TransactionRepository transactionRepository;


    public AppointmentServiceImpl(AppointmentRepository appointmentRepository,
                                  AppointmentMapper appointmentMapper,
                                  DoctorService doctorService,
                                  PatientService patientService,
                                  TransactionRepository transactionRepository) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentMapper = appointmentMapper;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public SearchPageAppointmentDto search(Integer pageNumber, Integer pageSize, AppointmentCriteria criteria) {
        Specification<Appointment> specification = AppointmentSpecification.findByCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page page = appointmentRepository.findAll(specification, pageable);

        List<Appointment> content = page.getContent();
        long totalCount = page.getTotalElements();

        SearchPageAppointmentDto dto = new SearchPageAppointmentDto(appointmentMapper.entityListToSearchDtoList(content), totalCount);
        return dto;
    }

    @Override
    public AppointmentDto save(Long patientId, Long doctorId, LocalDateTime appointmentStartDate, LocalDateTime appointmentEndDate) throws PatientNotFoundException, DoctorNotFoundException, UnAppropriateAppointmentException {

        if (appointmentStartDate.isAfter(appointmentEndDate) || appointmentEndDate.isBefore(LocalDateTime.now())) {
            throw new UnAppropriateAppointmentException();
        }

        Patient patient = patientService.getEntityById(patientId);
        Doctor doctor = doctorService.getEntityById(doctorId);

        Appointment appointment = new Appointment();
        appointment.setStartDate(appointmentStartDate);
        appointment.setEndDate(appointmentEndDate);
        appointment.setDoctor(doctor);
        appointment.setPatient(patient);
        appointment.setStatus(AppointmentStatusType.IN_PROGRESS);

        Duration duration = Duration.between(appointmentStartDate, appointmentEndDate);
        appointment.setAppointmentCost(duration.toHours() * doctor.getHourlyRate());

        appointmentRepository.save(appointment);

        AppointmentDto dto = appointmentMapper.entityToDto(appointment);
        return dto;
    }

    @Override
    public AppointmentDto getById(Long id) throws AppointmentNotFoundException {
        Appointment appointment = getEntityById(id);
        AppointmentDto dto = appointmentMapper.entityToDto(appointment);
        return dto;
    }

    @Override
    public AppointmentDto cancel(Long id) throws AppointmentNotFoundException, StartedAppointmentCantBeCancelledException, AlreadyCompletedCancelledAppointmentException {
        Appointment appointment = getEntityById(id);

        if (appointment.getStatus().equals(AppointmentStatusType.COMPLETE) || appointment.getStatus().equals(AppointmentStatusType.CANCEL)) {
            throw new AlreadyCompletedCancelledAppointmentException();
        }
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startDate = appointment.getStartDate();

        Duration duration = Duration.between(now, startDate);

        long minutes = duration.toMinutes();

        //1 hour check
        if (minutes > 0 && minutes < 61) {
            Transaction transaction = new Transaction();
            transaction.setCancelCost(true);
            transaction.setCost(appointment.getAppointmentCost() / 4);
            transaction.setAppointment(appointment);
            transactionRepository.save(transaction);
            appointment.setStatus(AppointmentStatusType.CANCEL);
        } else if (minutes < 0) {
            throw new StartedAppointmentCantBeCancelledException();
        } else {
            appointment.setStatus(AppointmentStatusType.CANCEL);
        }

        appointmentRepository.save(appointment);
        AppointmentDto dto = appointmentMapper.entityToDto(appointment);
        return dto;
    }

    @Override
    public AppointmentDto complete(Long id) throws AppointmentNotFoundException, AlreadyCompletedCancelledAppointmentException {
        Appointment appointment = getEntityById(id);

        if (appointment.getStatus().equals(AppointmentStatusType.COMPLETE) || appointment.getStatus().equals(AppointmentStatusType.CANCEL)) {
            throw new AlreadyCompletedCancelledAppointmentException();
        }

        appointment.setStatus(AppointmentStatusType.COMPLETE);
        Transaction transaction = new Transaction();
        transaction.setAppointment(appointment);
        transaction.setCost(appointment.getAppointmentCost());
        transaction.setCancelCost(false);

        appointmentRepository.save(appointment);
        transactionRepository.save(transaction);
        AppointmentDto dto = appointmentMapper.entityToDto(appointment);
        return dto;
    }

    @Override
    public Appointment getEntityById(Long id) throws AppointmentNotFoundException {
        Appointment appointment = appointmentRepository.findById(id).orElseThrow(AppointmentNotFoundException::new);
        return appointment;
    }

}
