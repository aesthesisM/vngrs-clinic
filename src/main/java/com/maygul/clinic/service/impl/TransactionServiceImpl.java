package com.maygul.clinic.service.impl;

import com.maygul.clinic.exception.TransactionNotFoundException;
import com.maygul.clinic.mapper.TransactionMapper;
import com.maygul.clinic.persistence.entity.Transaction;
import com.maygul.clinic.persistence.repository.TransactionRepository;
import com.maygul.clinic.persistence.specification.TransactionSpecification;
import com.maygul.clinic.persistence.specification.criteria.TransactionCriteria;
import com.maygul.clinic.service.TransactionService;
import com.maygul.clinic.service.dto.SearchPageTransactionDto;
import com.maygul.clinic.service.dto.TransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;
    private TransactionMapper transactionMapper;

    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  TransactionMapper transactionMapper) {
        this.transactionRepository = transactionRepository;
        this.transactionMapper = transactionMapper;
    }

    @Override
    public SearchPageTransactionDto search(Integer pageNumber, Integer pageSize, TransactionCriteria criteria) {
        Specification<Transaction> specification = TransactionSpecification.findByCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page page = transactionRepository.findAll(specification, pageable);

        List<Transaction> content = page.getContent();
        long totalCount = page.getTotalElements();

        SearchPageTransactionDto dto = new SearchPageTransactionDto(transactionMapper.entityListToSearchDtoList(content), totalCount);
        return dto;
    }

    @Override
    public TransactionDto getById(Long id) throws TransactionNotFoundException {
        Transaction transaction = transactionRepository.findById(id).orElseThrow(TransactionNotFoundException::new);
        TransactionDto dto = transactionMapper.entityToDto(transaction);
        return dto;
    }
}
