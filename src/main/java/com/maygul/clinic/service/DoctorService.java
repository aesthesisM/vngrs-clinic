package com.maygul.clinic.service;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.specification.criteria.DoctorCriteria;
import com.maygul.clinic.service.dto.DoctorDto;
import com.maygul.clinic.service.dto.SearchPageDoctorDto;

public interface DoctorService {

    SearchPageDoctorDto search(Integer pageNumber, Integer pageSize, DoctorCriteria criteria);

    DoctorDto getById(Long id) throws DoctorNotFoundException;

    DoctorDto save(String name, String speciality, Double hourlyRate);

    DoctorDto update(Long id, String name, String speciality, Double hourlyRate) throws DoctorNotFoundException;

    void delete(Long id) throws DoctorNotFoundException;

    Doctor getEntityById(Long id) throws DoctorNotFoundException;

}
