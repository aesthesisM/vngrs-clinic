package com.maygul.clinic.service;

import com.maygul.clinic.exception.*;
import com.maygul.clinic.persistence.entity.Appointment;
import com.maygul.clinic.persistence.specification.criteria.AppointmentCriteria;
import com.maygul.clinic.service.dto.AppointmentDto;
import com.maygul.clinic.service.dto.SearchPageAppointmentDto;

import java.time.LocalDateTime;

public interface AppointmentService {

    SearchPageAppointmentDto search(Integer pageNumber, Integer pageSize, AppointmentCriteria criteria);

    AppointmentDto save(Long patientId, Long doctorId, LocalDateTime appointmentStartDate, LocalDateTime appointmentEndDate) throws PatientNotFoundException, DoctorNotFoundException, UnAppropriateAppointmentException;

    AppointmentDto getById(Long id) throws AppointmentNotFoundException;

    AppointmentDto cancel(Long id) throws AppointmentNotFoundException, StartedAppointmentCantBeCancelledException, AlreadyCompletedCancelledAppointmentException;

    AppointmentDto complete(Long id) throws AppointmentNotFoundException, AlreadyCompletedCancelledAppointmentException;

    Appointment getEntityById(Long id) throws AppointmentNotFoundException;

}
