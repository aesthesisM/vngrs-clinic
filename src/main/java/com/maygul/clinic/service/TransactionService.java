package com.maygul.clinic.service;

import com.maygul.clinic.exception.TransactionNotFoundException;
import com.maygul.clinic.persistence.specification.criteria.TransactionCriteria;
import com.maygul.clinic.service.dto.SearchPageTransactionDto;
import com.maygul.clinic.service.dto.TransactionDto;

public interface TransactionService {
    
    SearchPageTransactionDto search(Integer pageNumber, Integer pageSize, TransactionCriteria criteria);

    TransactionDto getById(Long id) throws TransactionNotFoundException;
}
