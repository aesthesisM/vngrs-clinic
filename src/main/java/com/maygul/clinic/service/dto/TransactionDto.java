package com.maygul.clinic.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class TransactionDto {
    private Long id;
    private LocalDateTime createDat;
    private Double cost;
    private boolean isCancelCost;
}
