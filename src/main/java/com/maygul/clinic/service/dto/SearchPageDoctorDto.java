package com.maygul.clinic.service.dto;

import com.maygul.clinic.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPageDoctorDto extends BaseSearchPageDto<SearchDoctorDto> {

	public SearchPageDoctorDto(List<SearchDoctorDto> pageResponse, Long totalCount) {
		super(pageResponse, totalCount);
	}
}
