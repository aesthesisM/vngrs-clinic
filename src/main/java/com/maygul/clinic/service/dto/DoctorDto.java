package com.maygul.clinic.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoctorDto {
    private Long id;
    private String name;
    private String speciality;
    private Double hourlyRate;
}
