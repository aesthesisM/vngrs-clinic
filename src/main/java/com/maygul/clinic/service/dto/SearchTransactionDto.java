package com.maygul.clinic.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class SearchTransactionDto {
    private Long id;
    private LocalDateTime createDate;
    private Double cost;
    private boolean isCancelCost;
}
