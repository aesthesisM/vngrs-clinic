package com.maygul.clinic.service.dto;

import com.maygul.clinic.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPagePatientDto extends BaseSearchPageDto<SearchPatientDto> {

	public SearchPagePatientDto(
		List<SearchPatientDto> pageResponse, Long totalCount) {
		super(pageResponse, totalCount);
	}
}
