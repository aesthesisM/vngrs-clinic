package com.maygul.clinic.service.dto;

import com.maygul.clinic.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPageTransactionDto extends BaseSearchPageDto<SearchTransactionDto> {

	public SearchPageTransactionDto(
		List<SearchTransactionDto> pageResponse, Long totalCount) {
		super(pageResponse, totalCount);
	}
}
