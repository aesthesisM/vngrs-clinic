package com.maygul.clinic.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchDoctorDto {
    private Long id;
    private String name;
    private String speciality;
    private Long hourlyRate;
}
