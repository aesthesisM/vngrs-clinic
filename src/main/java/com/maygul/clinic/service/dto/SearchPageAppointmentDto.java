package com.maygul.clinic.service.dto;

import com.maygul.clinic.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPageAppointmentDto extends BaseSearchPageDto<SearchAppointmentDto> {

	public SearchPageAppointmentDto(
		List<SearchAppointmentDto> pageResponse, Long totalCount) {
		super(pageResponse, totalCount);
	}
}
