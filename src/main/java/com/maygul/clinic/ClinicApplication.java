package com.maygul.clinic;

import java.util.TimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicApplication {
  
  public static void main(String[] args) {
      TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
      SpringApplication.run(ClinicApplication.class, args);
  }
  
}
