package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.DataNotFoundException;

public class TransactionNotFoundException extends DataNotFoundException {
  public TransactionNotFoundException() {
    super(
        ClinicExceptionType.TRANSACTION_NOT_FOUND.getCode(),
        ClinicExceptionType.TRANSACTION_NOT_FOUND.getLabel()
    );
  }
}
