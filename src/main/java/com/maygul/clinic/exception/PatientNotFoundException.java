package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.DataNotFoundException;

public class PatientNotFoundException extends DataNotFoundException {
  public PatientNotFoundException() {
    super(
        ClinicExceptionType.PATIENT_NOT_FOUND.getCode(),
        ClinicExceptionType.PATIENT_NOT_FOUND.getLabel()
    );
  }
}
