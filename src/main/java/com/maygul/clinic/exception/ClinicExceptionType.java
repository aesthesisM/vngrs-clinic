package com.maygul.clinic.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClinicExceptionType {
  PATIENT_NOT_FOUND(1L, "com.maygul.clinic.exception.patient.not.found"),
  DOCTOR_NOT_FOUND(2L, "com.maygul.clinic.exception.doctor.not.found"),
  TRANSACTION_NOT_FOUND(3L, "com.maygul.clinic.exception.transaction.not.found"),
  APPOINTMENT_NOT_FOUND(4L, "com.maygul.clinic.exception.appointment.not.found"),
  INAPPROPRIATE_APPOINTMENT(5L,"com.maygul.clinic.exception.appointment.unappropriate.appointment"),
  STARTED_APPOINTMENT_CANT_BE_CANCELLED(6L,"com.maygul.clinic.exception.appointment.started.appointment.cant.be.cancelled"),
  ALREADY_CANCELLED_COMPLETED_APPOINTMENT(7L,"com.maygul.clinic.exception.appointment.already.completed.canceled");
  
  private Long code;
  private String label;
}
