package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.BusinessException;

public class UnAppropriateAppointmentException extends BusinessException {
    public UnAppropriateAppointmentException() {
        super(ClinicExceptionType.INAPPROPRIATE_APPOINTMENT.getCode(), ClinicExceptionType.INAPPROPRIATE_APPOINTMENT.getLabel());
    }
}
