package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.DataNotFoundException;

public class DoctorNotFoundException extends DataNotFoundException {
  public DoctorNotFoundException() {
    super(
        ClinicExceptionType.DOCTOR_NOT_FOUND.getCode(),
        ClinicExceptionType.DOCTOR_NOT_FOUND.getLabel()
    );
  }
}
