package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.BusinessException;

public class AlreadyCompletedCancelledAppointmentException extends BusinessException {
    public AlreadyCompletedCancelledAppointmentException() {
        super(ClinicExceptionType.ALREADY_CANCELLED_COMPLETED_APPOINTMENT.getCode(), ClinicExceptionType.ALREADY_CANCELLED_COMPLETED_APPOINTMENT.getLabel());
    }
}
