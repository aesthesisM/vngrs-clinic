package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.BusinessException;

public class StartedAppointmentCantBeCancelledException extends BusinessException {
    public StartedAppointmentCantBeCancelledException() {
        super(ClinicExceptionType.STARTED_APPOINTMENT_CANT_BE_CANCELLED.getCode(), ClinicExceptionType.STARTED_APPOINTMENT_CANT_BE_CANCELLED.getLabel());
    }
}
