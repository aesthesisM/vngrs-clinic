package com.maygul.clinic.exception;

import com.maygul.clinic.common.exception.type.DataNotFoundException;

public class AppointmentNotFoundException extends DataNotFoundException {
  public AppointmentNotFoundException() {
    super(
        ClinicExceptionType.APPOINTMENT_NOT_FOUND.getCode(),
        ClinicExceptionType.APPOINTMENT_NOT_FOUND.getLabel()
    );
  }
}
