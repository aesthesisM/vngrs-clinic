package com.maygul.clinic.persistence.entity;

public enum AppointmentStatusType {
    IN_PROGRESS, CANCEL, COMPLETE
}
