package com.maygul.clinic.persistence.entity;

public enum PatientType {
    PATIENT, DOCTOR
}
