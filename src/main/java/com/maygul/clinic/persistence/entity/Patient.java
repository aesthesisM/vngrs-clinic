package com.maygul.clinic.persistence.entity;

import com.maygul.clinic.common.persistence.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "PATIENT")
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_PATIENT")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Patient extends BaseEntity {

    private String name;

    @Column(name = "PATIENT_TYPE")
    @Enumerated(EnumType.STRING)
    private PatientType patientType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DOCTOR_ID", referencedColumnName = "ID")
    @LazyToOne(value = LazyToOneOption.NO_PROXY)
    private Doctor doctor;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "patient"
    )
    private List<Appointment> appointmentList;


    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void preUpdate() {
        setUpdateDate(LocalDateTime.now());
    }
}
