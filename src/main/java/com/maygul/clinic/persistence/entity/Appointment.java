package com.maygul.clinic.persistence.entity;

import com.maygul.clinic.common.persistence.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "APPOINTMENT")
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_APPOINTMENT")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Appointment extends BaseEntity {

    @Column(name = "APPOINTMENT_START_DATE")
    private LocalDateTime startDate;

    @Column(name = "APPOINTMENT_END_DATE")
    private LocalDateTime endDate;

    @OneToOne
    @JoinColumn(name = "DOCTOR_ID", referencedColumnName = "ID")
    private Doctor doctor;

    @OneToOne
    @JoinColumn(name = "PATIENT_ID", referencedColumnName = "ID")
    private Patient patient;

    @OneToOne(
            fetch = FetchType.LAZY,
            mappedBy = "appointment"
    )
    @LazyToOne(value = LazyToOneOption.NO_PROXY)
    private Transaction transaction;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private AppointmentStatusType status;

    @Column(name = "APPOINTMENT_COST")
    private Double appointmentCost;

    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void preUpdate() {
        setUpdateDate(LocalDateTime.now());
    }
}
