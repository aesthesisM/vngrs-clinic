package com.maygul.clinic.persistence.entity;

import com.maygul.clinic.common.persistence.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Table(name = "TRANSACTION")
@Entity
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_TRANSACTION")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Transaction extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "APPOINTMENT_ID", referencedColumnName = "ID")
    private Appointment appointment;

    @Column(name = "COST")
    private Double cost;

    @Column(name = "IS_CANCEL_COST")
    private boolean cancelCost;

    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void preUpdate() {
        setUpdateDate(LocalDateTime.now());
    }

}
