package com.maygul.clinic.persistence.entity;

import com.maygul.clinic.common.persistence.entity.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "DOCTOR")
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_DOCTOR")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Doctor extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "SPECIALITY")
    private String speciality;

    @Column(name = "HOURLY_RATE", precision = 3, scale = 2)
    private Double hourlyRate;

    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void preUpdate() {
        setUpdateDate(LocalDateTime.now());
    }
}
