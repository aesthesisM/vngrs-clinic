package com.maygul.clinic.persistence.specification;

import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.specification.criteria.PatientCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class PatientSpecification {
    public static Specification<Patient> findByCriteria(PatientCriteria criteria) {
        return (root, criteriaQuery, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();

            if(criteria.getName() != null){
                Predicate nameLikePredicate = cb.like(cb.lower(root.get("name")),"%"+criteria.getName()+"%");
                predicateList.add(nameLikePredicate);
            }
            return cb.and(predicateList.toArray(new Predicate[]{}));
        };
    }
}
