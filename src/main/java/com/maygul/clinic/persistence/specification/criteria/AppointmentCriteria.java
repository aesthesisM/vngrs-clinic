package com.maygul.clinic.persistence.specification.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class AppointmentCriteria {
	private Long doctorId;
	private Long patientId;
	private LocalDateTime appointmentStartDate;
	private LocalDateTime appointmentEndDate;
}
