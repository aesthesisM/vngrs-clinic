package com.maygul.clinic.persistence.specification;

import com.maygul.clinic.persistence.entity.Appointment;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.entity.Transaction;
import com.maygul.clinic.persistence.specification.criteria.TransactionCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class TransactionSpecification {
    public static Specification<Transaction> findByCriteria(TransactionCriteria criteria) {
        return (root, criteriaQuery, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();

            Join<Transaction, Appointment> appointmentJoin = null;
            if (criteria.getDoctorId() != null || criteria.getPatientId() != null) {
                appointmentJoin = root.join("appointment");
            }

            if (criteria.getTransactionStartDate() != null) {
                Predicate startDatePredicate = cb.greaterThanOrEqualTo(root.get("updateDate"), criteria.getTransactionStartDate());
                predicateList.add(startDatePredicate);
            }

            if (criteria.getTransactionEndDate() != null) {
                Predicate endDatePredicate = cb.lessThanOrEqualTo(root.get("updateDate"), criteria.getTransactionEndDate());
                predicateList.add(endDatePredicate);
            }

            if (criteria.getDoctorId() != null) {

                Join<Appointment, Doctor> doctorJoin = appointmentJoin.join("doctor");

                Predicate doctorPredicate = cb.equal(doctorJoin.get("id"), criteria.getDoctorId());
                predicateList.add(doctorPredicate);
            }

            if (criteria.getPatientId() != null) {
                Join<Appointment, Patient> patientJoin = appointmentJoin.join("patient");

                Predicate patientPredicate = cb.equal(patientJoin.get("id"), criteria.getPatientId());
                predicateList.add(patientPredicate);
            }

            return cb.and(predicateList.toArray(new Predicate[]{}));
        };
    }
}
