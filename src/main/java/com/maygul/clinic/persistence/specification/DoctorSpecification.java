package com.maygul.clinic.persistence.specification;

import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.specification.criteria.DoctorCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class DoctorSpecification {
    public static Specification<Doctor> findByCriteria(DoctorCriteria criteria) {
        return (root, criteriaQuery, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();

            if (criteria.getName() != null) {
                Predicate nameLikePredicate = cb.like(cb.lower(root.get("name")), "%" + criteria.getName() + "%");
                predicateList.add(nameLikePredicate);
            }

            if (criteria.getSpeciality() != null) {
                Predicate specialityPredicate = cb.equal(root.get("speciality"), criteria.getSpeciality());
                predicateList.add(specialityPredicate);
            }

            if (criteria.getHourlyRate() != null) {
                Predicate hourlyRatePredicate = cb.equal(root.get("hourlyRate"), criteria.getHourlyRate());
                predicateList.add(hourlyRatePredicate);
            }
            return cb.and(predicateList.toArray(new Predicate[]{}));
        };
    }
}
