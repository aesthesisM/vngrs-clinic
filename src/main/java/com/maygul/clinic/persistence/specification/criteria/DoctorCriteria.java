package com.maygul.clinic.persistence.specification.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DoctorCriteria {
    private String name;
    private String speciality;
    private Double hourlyRate;
}
