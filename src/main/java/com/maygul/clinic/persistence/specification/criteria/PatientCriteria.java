package com.maygul.clinic.persistence.specification.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PatientCriteria {
    private String name;
}
