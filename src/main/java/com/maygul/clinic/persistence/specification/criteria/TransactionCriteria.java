package com.maygul.clinic.persistence.specification.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class TransactionCriteria {
    private Long doctorId;
    private Long patientId;
    private LocalDateTime transactionStartDate;
    private LocalDateTime transactionEndDate;
}
