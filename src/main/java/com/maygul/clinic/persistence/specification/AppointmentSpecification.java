package com.maygul.clinic.persistence.specification;

import com.maygul.clinic.persistence.entity.Appointment;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.specification.criteria.AppointmentCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class AppointmentSpecification {
    public static Specification<Appointment> findByCriteria(AppointmentCriteria criteria) {
        return (root, criteriaQuery, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();

            if (criteria.getDoctorId() != null) {
                Join<Appointment, Doctor> doctorJoin = root.join("doctor");
                Predicate doctorPredicate = cb.equal(doctorJoin.get("id"), criteria.getDoctorId());
                predicateList.add(doctorPredicate);
            }

            if (criteria.getPatientId() != null) {
                Join<Appointment, Patient> patientJoin = root.join("patient");
                Predicate patientPredicate = cb.equal(patientJoin.get("id"), criteria.getDoctorId());
                predicateList.add(patientPredicate);
            }

            if (criteria.getAppointmentStartDate() != null) {
                Predicate startDatePredicate = cb.greaterThanOrEqualTo(root.get("startDate"), criteria.getAppointmentStartDate());
                predicateList.add(startDatePredicate);
            }

            if (criteria.getAppointmentEndDate() != null) {
                Predicate endDatePredicate = cb.lessThanOrEqualTo(root.get("endDate"), criteria.getAppointmentEndDate());
                predicateList.add(endDatePredicate);
            }
            return cb.and(predicateList.toArray(new Predicate[]{}));
        };
    }
}
