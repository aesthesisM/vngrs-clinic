package com.maygul.clinic.api;

import com.maygul.clinic.exception.*;
import com.maygul.clinic.persistence.specification.criteria.AppointmentCriteria;
import com.maygul.clinic.service.AppointmentService;
import com.maygul.clinic.service.dto.AppointmentDto;
import com.maygul.clinic.service.dto.SearchPageAppointmentDto;
import com.sun.istack.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    private AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping
    public ResponseEntity<SearchPageAppointmentDto> search(
            @RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "doctorId", required = false) Long doctorId,
            @RequestParam(name = "patientId", required = false) Long patientId,
            @RequestParam(name = "appointmentStartDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime appointmentStartDate,
            @RequestParam(name = "appointmentEndDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime appointmentEndDate
    ) {

        AppointmentCriteria criteria = new AppointmentCriteria(doctorId, patientId, appointmentStartDate, appointmentEndDate);
        SearchPageAppointmentDto dto = appointmentService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppointmentDto> getById(@PathVariable("id") Long id) throws AppointmentNotFoundException {
        AppointmentDto dto = appointmentService.getById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<AppointmentDto> save(
            @RequestParam(name = "patientId") @NotNull Long patientId,
            @RequestParam(name = "doctorId") @NotNull Long doctorId,
            @RequestParam(name = "appointmentStartDate") @NotNull @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime appointmentStartDate,
            @RequestParam(name = "appointmentEndDate") @NotNull @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime appointmentEndDate
    ) throws DoctorNotFoundException, PatientNotFoundException, UnAppropriateAppointmentException {
        AppointmentDto dto = appointmentService.save(patientId, doctorId, appointmentStartDate, appointmentEndDate);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/cancel/{id}")
    public ResponseEntity<AppointmentDto> cancel(@PathVariable("id") Long id) throws StartedAppointmentCantBeCancelledException, AppointmentNotFoundException, AlreadyCompletedCancelledAppointmentException {
        AppointmentDto dto = appointmentService.cancel(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/complete/{id}")
    public ResponseEntity<AppointmentDto> complete(@PathVariable("id") Long id) throws AlreadyCompletedCancelledAppointmentException, AppointmentNotFoundException {
        AppointmentDto dto = appointmentService.complete(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
