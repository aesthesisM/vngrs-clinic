package com.maygul.clinic.api;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.exception.PatientNotFoundException;
import com.maygul.clinic.persistence.specification.criteria.PatientCriteria;
import com.maygul.clinic.service.PatientService;
import com.maygul.clinic.service.dto.PatientDto;
import com.maygul.clinic.service.dto.SearchPagePatientDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patients")
public class PatientController {

    private PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping
    public ResponseEntity<SearchPagePatientDto> search(@RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
                                                       @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                       @RequestParam(name = "name", required = false) String name) {
        PatientCriteria criteria = new PatientCriteria(name);
        SearchPagePatientDto dto = patientService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PatientDto> getById(@PathVariable("id") Long id) throws PatientNotFoundException {
        PatientDto dto = patientService.getById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<PatientDto> save(@RequestParam(name = "name") String name,
                                           @RequestParam(name = "isPatientDoctor", defaultValue = "false") boolean isPatientDoctor,
                                           @RequestParam(name = "doctorId",required = false) Long doctorId) throws DoctorNotFoundException {
        PatientDto dto = patientService.save(name, isPatientDoctor, doctorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<PatientDto> update(@PathVariable("id") Long id,
                                             @RequestParam(name = "name") String name) throws PatientNotFoundException {
        PatientDto dto = patientService.update(id, name);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) throws PatientNotFoundException {
        patientService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
