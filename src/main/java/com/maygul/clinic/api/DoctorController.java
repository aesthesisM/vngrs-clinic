package com.maygul.clinic.api;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.persistence.specification.criteria.DoctorCriteria;
import com.maygul.clinic.service.DoctorService;
import com.maygul.clinic.service.dto.DoctorDto;
import com.maygul.clinic.service.dto.SearchPageDoctorDto;
import com.sun.istack.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/doctors")
public class DoctorController {

    private DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping
    public ResponseEntity<SearchPageDoctorDto> search(
            @RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "speciality",required = false) String speciality,
            @RequestParam(name = "hourlyRate", required = false) Double hourlyRate
    ) {
        DoctorCriteria criteria = new DoctorCriteria(name, speciality,hourlyRate);
        SearchPageDoctorDto dto = doctorService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DoctorDto> getById(@PathVariable("id") Long id) throws DoctorNotFoundException {
        DoctorDto dto = doctorService.getById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<DoctorDto> save(
            @RequestParam(name = "name") @NotNull String name,
            @RequestParam(name = "speciality") @NotNull String speciality,
            @RequestParam(name = "hourlyRate") @NotNull Double hourlyRate
    ) {
        DoctorDto dto = doctorService.save(name, speciality, hourlyRate);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<DoctorDto> update(
            @PathVariable("id") Long id,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "speciality", required = false) String speciality,
            @RequestParam(name = "hourlyRate", required = false) Double hourlyRate
    ) throws DoctorNotFoundException {
        DoctorDto dto = doctorService.update(id, name, speciality, hourlyRate);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) throws DoctorNotFoundException {
        doctorService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
