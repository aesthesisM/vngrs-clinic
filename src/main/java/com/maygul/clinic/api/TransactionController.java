package com.maygul.clinic.api;

import com.maygul.clinic.exception.TransactionNotFoundException;
import com.maygul.clinic.persistence.specification.criteria.TransactionCriteria;
import com.maygul.clinic.service.TransactionService;
import com.maygul.clinic.service.dto.SearchPageTransactionDto;
import com.maygul.clinic.service.dto.TransactionDto;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public ResponseEntity<SearchPageTransactionDto> search(
            @RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "doctorId", required = false) Long doctorId,
            @RequestParam(name = "patientId", required = false) Long patientId,
            @RequestParam(name = "transactionStartDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime transactionStartDate,
            @RequestParam(name = "transactionEndDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime transactionEndDate
    ) {
        TransactionCriteria criteria = new TransactionCriteria(doctorId, patientId, transactionStartDate, transactionEndDate);
        SearchPageTransactionDto dto = transactionService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionDto> getById(@PathVariable("id") Long id) throws TransactionNotFoundException {
        TransactionDto dto = transactionService.getById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
