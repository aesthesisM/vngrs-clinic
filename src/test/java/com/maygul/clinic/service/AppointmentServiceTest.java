package com.maygul.clinic.service;

import com.maygul.clinic.exception.*;
import com.maygul.clinic.mapper.AppointmentMapper;
import com.maygul.clinic.persistence.entity.*;
import com.maygul.clinic.persistence.repository.AppointmentRepository;
import com.maygul.clinic.persistence.repository.TransactionRepository;
import com.maygul.clinic.persistence.specification.criteria.AppointmentCriteria;
import com.maygul.clinic.service.dto.SearchPageAppointmentDto;
import com.maygul.clinic.service.impl.AppointmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceTest {

    @InjectMocks
    private AppointmentServiceImpl appointmentService;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Mock
    private AppointmentMapper appointmentMapper;

    @Mock
    private DoctorService doctorService;

    @Mock
    private PatientService patientService;

    @Mock
    private TransactionRepository transactionRepository;

    @Test
    void givenEmptyPage_whenSearch_thenShouldReturnEmpty() {
        //given
        AppointmentCriteria criteria = new AppointmentCriteria(1l, null, null, null);
        //when
        when(appointmentRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(Page.empty());
        SearchPageAppointmentDto dto = appointmentService.search(0, 10, criteria);
        //then
        Assertions.assertEquals(dto.getTotalCount(), 0l);
    }

    @Test
    void givenAppointment_whenSave_thenVerifySaveMethodIsCalled() throws DoctorNotFoundException, UnAppropriateAppointmentException, PatientNotFoundException {
        //given
        Long patientId = 1l;
        Long doctorId = 1121l;
        LocalDateTime startDate = LocalDateTime.now().plusHours(5);
        LocalDateTime endTime = LocalDateTime.now().plusHours(7);

        Doctor doctor = Doctor.builder().hourlyRate(20.0).name("doctor-1").speciality("backend").build();
        Patient patient = Patient.builder().patientType(PatientType.PATIENT).name("patient-1").doctor(null).build();
        doctor.setId(doctorId);
        patient.setId(patientId);
        //when
        when(doctorService.getEntityById(anyLong())).thenReturn(doctor);
        when(patientService.getEntityById(anyLong())).thenReturn(patient);
        appointmentService.save(patientId, doctorId, startDate, endTime);
        //then
        verify(appointmentRepository, times(1)).save(any(Appointment.class));
    }

    @Test
    void givenAppointment_whenSave_thenThrowExceptionForStartEndDate() {
        //given
        LocalDateTime startDate = LocalDateTime.now().plusHours(7);
        LocalDateTime endTime = LocalDateTime.now().plusHours(5);
        //then
        assertThrows(UnAppropriateAppointmentException.class, () ->
                        appointmentService.save(1l, 1l, startDate, endTime),
                "Expected UnAppropriateAppointmentException.class but it didn't show up"
        );
    }

    @Test
    void givenAppointment_checkAppointmentCanBeCancelled_thenExpectAlreadyCompletedCanceledException() {
        //given
        //when
        Appointment appointment = Appointment.builder()
                .status(AppointmentStatusType.CANCEL)
                .build();

        when(appointmentRepository.findById(anyLong())).thenReturn(Optional.of(appointment));
        //then
        assertThrows(AlreadyCompletedCancelledAppointmentException.class,
                () -> appointmentService.cancel(1L),
                "AlreadyCompletedCancelled exception should be thrown but not");

    }

    @Test
    void givenAppointment_whenCancel_thenExpectStartedAppointmentCantBeCancelledException() throws AlreadyCompletedCancelledAppointmentException, AppointmentNotFoundException, StartedAppointmentCantBeCancelledException {
        //given
        Appointment appointment = Appointment.builder()
                .status(AppointmentStatusType.IN_PROGRESS)
                .startDate(LocalDateTime.now().minusMinutes(5))
                .endDate(LocalDateTime.now().plusHours(1))
                .build();
        appointment.setId(1l);
        //when
        when(appointmentRepository.findById(anyLong())).thenReturn(Optional.of(appointment));
        //then
        assertThrows(StartedAppointmentCantBeCancelledException.class,
                () -> appointmentService.cancel(1l),
                "StartedAppointmentCantBeCancelledException should be thrown");
    }

    @Test
    void givenAppointment_whenCancel_thenCheckTransactionCost() throws AlreadyCompletedCancelledAppointmentException, AppointmentNotFoundException, StartedAppointmentCantBeCancelledException {
        //given
        Double cost = 500.0;
        Appointment appointment = Appointment.builder()
                .status(AppointmentStatusType.IN_PROGRESS)
                .startDate(LocalDateTime.now().plusMinutes(20))
                .endDate(LocalDateTime.now().plusHours(1))
                .appointmentCost(cost)
                .build();
        appointment.setId(1l);

        //when
        when(appointmentRepository.findById(anyLong())).thenReturn(Optional.of(appointment));
        appointmentService.cancel(1l);
        //then
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository).save(transactionArgumentCaptor.capture());

        assertEquals(cost/4,transactionArgumentCaptor.getValue().getCost());
    }


    @Test
    void givenAppointment_whenComplete_thenCheckTransactionCreationAndCost() throws AlreadyCompletedCancelledAppointmentException, AppointmentNotFoundException, StartedAppointmentCantBeCancelledException {
        //given
        Double cost = 250.0;
        Appointment appointment = Appointment.builder()
                .status(AppointmentStatusType.IN_PROGRESS)
                .startDate(LocalDateTime.now().plusHours(5))
                .endDate(LocalDateTime.now().plusHours(10))
                .appointmentCost(cost)
                .build();
        //when
        when(appointmentRepository.findById(anyLong())).thenReturn(Optional.of(appointment));
        appointmentService.complete(1L);
        //then
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository).save(transactionArgumentCaptor.capture());

        verify(transactionRepository,atLeastOnce()).save(any(Transaction.class));

        assertEquals(cost,transactionArgumentCaptor.getValue().getCost());
    }


}