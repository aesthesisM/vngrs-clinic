package com.maygul.clinic.service;

import com.maygul.clinic.mapper.DoctorMapper;
import com.maygul.clinic.persistence.entity.Doctor;
import com.maygul.clinic.persistence.repository.DoctorRepository;
import com.maygul.clinic.service.impl.DoctorServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class DoctorServiceTest {

    @InjectMocks
    private DoctorServiceImpl doctorService;

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private DoctorMapper doctorMapper;

    @Test
    void givenDoctor_whenSave_thenCheckIfDoctorSaved(){
        //given
        String name = "doctor bambam";
        String speciality = "bambam";
        Double hourlyRate = Double.MIN_VALUE;
        //when
        doctorService.save(name,speciality,hourlyRate);
        //
        verify(doctorRepository,times(1)).save(any(Doctor.class));
    }
}
