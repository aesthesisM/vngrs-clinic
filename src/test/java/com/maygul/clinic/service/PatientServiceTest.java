package com.maygul.clinic.service;

import com.maygul.clinic.exception.DoctorNotFoundException;
import com.maygul.clinic.exception.PatientNotFoundException;
import com.maygul.clinic.mapper.PatientMapper;
import com.maygul.clinic.persistence.entity.Patient;
import com.maygul.clinic.persistence.entity.PatientType;
import com.maygul.clinic.persistence.repository.PatientRepository;
import com.maygul.clinic.service.impl.PatientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTest {

    @InjectMocks
    private PatientServiceImpl patientService;

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private PatientMapper patientMapper;

    @Mock
    private DoctorService doctorService;


    @Test
    void givenNonDoctorPatient_whenSave_thenPatientShouldBeSaved() throws DoctorNotFoundException {
        //given
        String name = "patient bambam";
        boolean isDoctor = false;
        Long doctorId = null;
        //when
        patientService.save(name,isDoctor,doctorId);
        //then
        verify(patientRepository,atLeastOnce()).save(any(Patient.class));
    }

    @Test
    void givenDoctorPatient_whenSave_thenExpectDoctorNotFoundException() throws DoctorNotFoundException {
        //given
        String name = "patient bambam";
        boolean isDoctor = true;
        Long doctorId = 1231321L;
        //when
        when(doctorService.getEntityById(anyLong())).thenThrow(DoctorNotFoundException.class);
        //then
        assertThrows(DoctorNotFoundException.class,
                ()->patientService.save(name,isDoctor,doctorId),
                "DoctorNotFoundException should thrown");
    }

    @Test
    void givenPatient_whenUpdate_thenCheckIfSaveIsCalled() throws PatientNotFoundException {
        //given
        Patient patient = Patient.builder()
                .patientType(PatientType.PATIENT)
                .name("patient BamBam")
                .build();
        patient.setId(123l);
        //when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        //then
        patientService.update(1l,"patient bambam update");
        verify(patientRepository,times(1)).save(patient);
    }

    @Test
    void givenPatient_whenDelete_thenCheckIfDeleteIsCalled() throws PatientNotFoundException {
        //given
        Patient patient = Patient.builder()
                .patientType(PatientType.PATIENT)
                .name("delete bambam")
                .build();
        patient.setId(225l);
        //when
        when(patientRepository.findById(anyLong())).thenReturn(Optional.of(patient));
        //then
        patientService.delete(12313123l);
        verify(patientRepository,times(1)).delete(patient);
    }
}
