# README #


To build project run the following command

mvn clean install 

To run project

mwnw spring-boot:run 
or
cd target
java -jar clinic-1.0.0.jar

It only ıncludes Appointment class's test cases. I've don't have enough time for it. Even though I said 5 days I was on vacation.

common packages includes ExceptionHandler and basic parent classes for both entity and dtos
api packages contains controllers
exception packages contains exceptions which is thrown by clinic application
persistence packages contains entities, repositories, specifications and criterias
service methods includes service contracts, implementations and dtos
mapper includes mapper contracts

For the case, which doctor became a patient is handled by creating patient with doctor information. Other than that there is no logic to this.
Transaction is created after complete/cancel process.


What could be done to make it better ? Well response and request objects could be written. Code coverage could be %60 percent if other classes test cases would be implemented. docker-compose vs could be implemented.